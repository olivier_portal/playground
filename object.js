import {getHostel, getHostelObject} from './hotels.data';

// faire tous les exos de la page arrays en utilisant object.keys ou object.values

// exercice 0 : mettre une majuscule à toutes les RoomName

console.log('[OBJECT]Exo0 : ---------------------------');

function capi(name) {
  return name.charAt(0).toUpperCase() + name.slice(1);
}

const newArray = Object.values(getHostelObject());

const cleanedArray = newArray.map((hostel) => {
  hostel.rooms = Object.values(hostel.rooms);
  return hostel;
});

function capiAllRooms() {
  return cleanedArray.map((hostel) => {
    hostel.rooms.map((room) => {
      room.roomName = capi(room.roomName);
      return room;
    });
    return hostel;
  });
}
console.log(capiAllRooms(getHostelObject()), getHostelObject());

// exercice 1 : trier les hotels par nombre
// de chambres (plus grand en 1er) et créer un tableau
// contenant seulement
// le nom des hotels dans leur ordre de tri

console.log('[OBJECT]Exo1 : ---------------------------');

const newArray1 = Object.values(getHostelObject())
    .sort((a, b) => b.roomNumbers - a.roomNumbers)
    .map((hostel) => hostel.name);

console.log(newArray1);

// exercice 2 : faire un tableau avec toutes les chambres de tous les hotels,
// et ne garder que les chambres qui
// ont plus que 3 places ou exactement 3 places et
// les classer par ordre alphabétique selon le non de la chambre

console.log('[OBJECT]Exo2 : ---------------------------');

const newArray2 = Object.values(getHostelObject())
    .reduce((acc, hostel) => acc.concat(Object.values(hostel.rooms)), [])
    .filter((room) => room.size >= 3)
    .sort((a, b) => b.roomName < a.roomName ? 1 : -1);

console.log(newArray2);

// exercice 3 : faire un tableau avec toutes les chambres des hotels
// et ne garder que les chambres qui ont plus de 3 places et dont le
// nom de chambre a une taille supérieure à 15 charactères

console.log('[OBJECT]Exo3 : ---------------------------');

const newArray3 = Object.values(getHostelObject())
    .reduce((acc, hostel) => acc.concat(Object.values(hostel.rooms)), [])
    .filter((room) => room.size > 3 && room.roomName.length > 15);

console.log(newArray3);

// exercice 4 : enlever de la liste des hotels toutes les chambres qui ont plus
// de 3 places et changer la valeur de roomNumbers pour qu'elle reflete
// le nouveau nombre de chambres

console.log('[OBJECT]Exo4 : ---------------------------');

const newArray4 = Object.values(getHostelObject()).map((hostel) => {
  hostel.rooms = Object.values(hostel.rooms)
      .filter((room) => room.size < 4);
  hostel.roomNumbers = hostel.rooms.length;
  return hostel;
});

console.log(newArray4);

// exercice 5  : extraire du tableau hostels l'hotel qui a le nom 'hotel ocean' en le supprimant du tableau,
// et le mettre dans une nouvelle variable
// puis effacer toutes ses chambres et mettre à jour sa valeur room number, puis pusher l'hotel modifié dans hostels
// puis faire un sort par nom d'hotel
// puis donner le nouvel index de l'hotel océan (faire 2 méthodes : avec indexOf et avec un foreach)

console.log('[OBJECT]Exo5 : ---------------------------');

const newArray5 = Object.values((getHostelObject()));

const findHostel = newArray5
    .findIndex((hostel) => hostel.name === 'hotel ocean');

const [hostelToExtract] = newArray5
    .splice(findHostel, 1);

hostelToExtract.rooms = [];
hostelToExtract.roomNumbers = hostelToExtract.rooms.length;

newArray5
    .push(hostelToExtract);

newArray5
    .sort((a, b) => b.name < a.name ? 1 : -1);

const newIndexOcean = newArray5
    .findIndex((hostel) => hostel.name === 'hotel ocean');

console.log(hostelToExtract, newArray5, newIndexOcean, getHostelObject());

// exercice 6 : créer un objet dont les clés sont le nom des hotels et dont la valeur est un booléen qui indique
// si l'hotel a une chambre qui s'appelle 'suite marseillaise'

console.log('[OBJECT]Exo6 : ---------------------------');

const keys = {};

const newArray6 = Object.values(getHostelObject())
    .map((hostel) => {
      keys[hostel.name] = Object.values(hostel.rooms)
          .some((room) => room.roomName === 'suite marseillaise');
      return hostel;
    });

console.log(newArray6);

// exercice 7 : faire une fonction qui prend en paramètre un id d'hotel et qui retourne son nom

console.log('[OBJECT]Exo7 : ---------------------------');

function getHostelNameById(hostelId) {
  const hostelToFind = Object.values(getHostelObject()).find((hostel) => hostel.id === hostelId);
  if (!hostelToFind) {
    return `Cet hôtel n'existe pas !`;
  } return hostelToFind.name;
}

console.log(getHostelNameById(1), getHostelObject());

// exercice 8 : faire une fonction qui prend en paramètre un id d'hotel et un id de chambre et qui retourne son nom

console.log('[OBJECT]Exo8 : ---------------------------');

function getRoomsByHostelAndRoomId(myHostel, hostelId, roomId) {
  const hostelTofind = Object.values(myHostel).find((hostel) => hostel.id === hostelId);
  if (!hostelTofind) {
    return `Cet hôtel n'existe pas !`;
  }
  const roomToFind = Object.values(hostelTofind.rooms).find((room) => room.id === roomId);
  if (!roomToFind) {
    return `Cette chambre n'existe pas !`;
  }
  return roomToFind.roomName;
}

console.log(getRoomsByHostelAndRoomId(getHostelObject(), 1, 4));

// exercice 9: faire une fonction qui prend en paramètre la liste des hotels
// et qui vérifie que toutes les chambres des hotels ont bien
// une majuscule a leur nom et qui renvoie un boolean donnant le résultat.
// Puis faire en sorte que la liste des hotels valide bien cette fonction.

console.log('[OBJECT]Exo9 : ---------------------------');


function verifyAllRoomNameAreCapi(hostels) {
  return Object.values(hostels)
      .every((hostel) => Object.values(hostel.rooms)
          .every((room) => room.roomName === capi(room.roomName)));
}

console.log(verifyAllRoomNameAreCapi(getHostelObject()), getHostelObject());
console.log(verifyAllRoomNameAreCapi(capiAllRooms(getHostelObject())), capiAllRooms(getHostelObject()));

// exercice 10 : faire une fonction qui prend en paramètre la liste des hotels
// et qui renvoie un tableau contenant les id des hotels
// qui ont au moins une chambre avec plus ou exactement 5 places

console.log('[OBJECT]Exo10 : ---------------------------');

function haveMoreThan5(hostel) {
  return hostel.rooms = Object.values(hostel.rooms)
      .some((room) => room.size >= 5);
}

function getHostelsIdForMoreThan5(hostels) {
  return Object.values(hostels).reduce((acc, hostel) => {
    if (haveMoreThan5(hostel)) {
      return acc.concat(hostel.id);
    } return acc;
  }, []);
}

console.log(getHostelsIdForMoreThan5(getHostelObject()));

// exercice 11 : faire une fonction qui prend en paramètre la liste des hotels
// et qui renvoie dans un tableau toutes les chambres qui ont plus de 3 places
// et qui sont dans un hotel avec piscine
// puis qui ajoute à chaque chambre une propriété "hotelName"
// qui contient le nom de l'hotel à laquelle elle appartient

console.log('[OBJECT]Exo11 : ---------------------------');

function getAllHostelsWithMoreThan3AndAPool(hostels) {
  return Object.values(hostels)
      .filter((hostel) => hostel.pool === true)
      .reduce((acc, hostel) => {
        hostel.rooms = Object.values(hostel.rooms)
            .filter((room) => room.size > 3)
            .map((room) => {
              room.hostelName = hostel.name;
              return room;
            });
        return acc.concat(hostel.rooms);
      }, []);
}

console.log(getAllHostelsWithMoreThan3AndAPool(getHostelObject()));

// exercice 12 : faire une fonction qui prend en paramètre la liste des hotel,
// un id d'hotel et une taille et qui renvoie le nom et l'id de toutes
// les chambres de cet hotel qui font exactement la taille indiquée

console.log('[OBJECT]Exo12 : ---------------------------');

function getNameAndIdOfAllRooms(hostels, hostelId, roomSize) {
  const hostelsArray = Object.values(hostels);
  const hostelsWithRoomsArray = hostelsArray.map((hostel) => {
    hostel.rooms = Object.values(hostel.rooms);
    return hostel;
  });
  const hostelToFind = hostelsWithRoomsArray.find((hostel) => hostel.id === hostelId);
  if (!hostelToFind) {
    return `Cet hôtel n'existe pas !`;
  }
  const roomSizeToFind = hostelToFind.rooms.find((room) => room.size === roomSize);
  if (!roomSizeToFind) {
    return `Cet hôtel n'a pas de chambre de cette taille`;
  }
  return hostelToFind.rooms
      .filter((room) => room.size === roomSize)
      .map((room) => ({id: room.id, roomName: room.roomName}));
}

console.log(getNameAndIdOfAllRooms(getHostelObject(), 1, 2));

// exercice 13 : faire une fonction qui prend en paramètre la liste des hotel et
// un id d'hotel et qui supprime cet hotel de la liste des hotels

console.log('[OBJECT]Exo13 : ---------------------------');

function suppressHostelFromList(hostels, hostelId) {
  const hostelToFind = Object.values(hostels).find((hostel) => hostel.id === hostelId);
  if (!hostelToFind) {
    return `Cet hôtel n'existe pas!`;
  } return Object.values(hostels)
      .filter((hostel) => hostel.id !== hostelId);
}

console.log(suppressHostelFromList(getHostelObject(), 2));

// exercice 14 : faire une fonction qui prend en paramètre la liste des hotel et
// un id d'hotel et un id de chambre et qui supprime cet chambre de l'hotel concerné

console.log('[OBJECT]Exo14 : ---------------------------');

function suppressRoomsFromList(hostels, hostelId, roomId) {
  const newArray = Object.values(getHostelObject());

  const cleanedArray = newArray.map((hostel) => {
    hostel.rooms = Object.values(hostel.rooms);
    return hostel;
  });

  const hostelToFind = cleanedArray.find((hostel) => hostel.id === hostelId);
  if (!hostelToFind) {
    return `Cet hôtel n'existe pas!`;
  }

  const roomToFind = hostelToFind.rooms.find((room) => room.id === roomId);
  if (!roomToFind) {
    return `Cette chambre n'existe pas!`;
  }
  hostelToFind.rooms = hostelToFind.rooms
      .filter((room) => room.id !== roomId);
  hostelToFind.roomNumbers = hostelToFind.rooms.length;
  return hostelToFind;
}

console.log(suppressRoomsFromList(getHostelObject(), 3, 5));

// exercice 15 : faire une fonction qui prend en paramètre la liste des hotels,
// une id d'hotel et un boolean et qui va changer la valeur de l'attribut "pool"
// dans l'hotel concerné avec le boolean donné

console.log('[OBJECT]Exo15 : ---------------------------');

function changePoolValue(hostels, hostelId, poolValue) {
  const hostelToFind = Object.values(hostels)
      .find((hostel) => hostel.id === hostelId);
  if (!hostelToFind) {
    return `Cet hôtel n'existe pas!`;
  }
  hostelToFind.pool = poolValue;
  return hostelToFind;
}

console.log(changePoolValue(getHostelObject(), 2, true), getHostel());

// exercice 16 : faire une fonction qui prend en paramètre la liste des hotels,
// une id d'hotel et une nouvelle chambre (avec son nom et sa taille) qui
// ajoute la nouvelle chambre dans l'hotel concerné et lui donne un id qui suit le
// dernier id de l'hotel

console.log('[OBJECT]Exo16 : ---------------------------');

function addNewRoom(hostels, hostelId, newRoom) {
  const newArray = Object.values(getHostelObject());

  const cleanedArray = newArray.map((hostel) => {
    hostel.rooms = Object.values(hostel.rooms);
    return hostel;
  });
  const hostelToFind = cleanedArray
      .find((hostel) => hostel.id === hostelId);
  if (!hostelToFind) {
    return `Cet hôtel n'existe pas!`;
  } hostelToFind.rooms
      .push({...newRoom, id: hostelToFind.rooms.length + 1});
  hostelToFind.roomNumbers = hostelToFind.rooms.length;
  return hostelToFind;
}

const newRoom = {
  roomName: 'Ma nouvelle chambre',
  size: 10,
};

console.log(addNewRoom(getHostelObject(), 3, newRoom));

// exercice 17 : faire une fonction qui prend en paramètre la liste des hotels,
// une id d'hotel, un id de chambre et un nouveau nom de chambre qui va changer le nom
// de la chambre indiquée par le nouveau nom

console.log('[OBJECT]Exo17 : ---------------------------');

function changeRoomName(hostels, hostelId, roomId, newRoomName) {
  const newArray = Object.values(getHostelObject());

  const cleanedArray = newArray.map((hostel) => {
    hostel.rooms = Object.values(hostel.rooms);
    return hostel;
  });

  const hostelToFind = cleanedArray.find((hostel) => hostel.id === hostelId);
  if (!hostelToFind) {
    return `Cet hôtel n'existe pas!`;
  }

  const roomToFind = hostelToFind.rooms.find((room) => room.id === roomId);
  if (!roomToFind) {
    return `Cette chambre n'existe pas!`;
  }
  roomToFind.roomName = newRoomName;
  return hostelToFind;
}

console.log(changeRoomName(getHostelObject(), 3, 5, 'Mon nouveau nom de chambre'));

const lessons = {
  lesson1: true,
  lesson11: false,
  lesson2: true,
  lesson15: false,
  lesson3: true,
  lesson4: true,
  lesson13: false,
  lesson6: false,
  lesson12: false,
  lesson7: false,
  lesson8: false,
  lesson9: false,
  lesson10: false,
  lesson5: false,
  lesson14: false,
};

// exercice 1 : changer cet objet pour que ses clés soient classées dans l'ordre alphabétique

function sortLesson() {
  const newKeys = Object.keys(lessons);
  const tab = [];
  for (let i = 0; i <= newKeys.length - 1; i++) {
    if (newKeys[i].length === 7) {
      newKeys[i] = 'lesson' + 0 + newKeys[i].substring(6, newKeys.length);
    } else {
      newKeys[i] = 'lesson' + newKeys[i].substring(6, newKeys.length);
    }
    tab.push({lessonNumber: newKeys[i]});
    tab.sort((a, b) => b.lessonNumber < a.lessonNumber ? 1 : -1);
  }
  return tab;
}

console.log(sortLesson());

// exercice 2 : en gardant l'objet trié, faites une fonction qui
// donne le numéro de la première clé qui a la valeur false
// sous la forme {key: 4, value: false}

// exercice 1 : créer une fonction qui prend en paramètre un nombre entier et qui
// affiche tous les entiers entre 0 et lui même

console.log('[BOUCLE FOR]: Exo1--------------');

function generateIntFrom0ToIntNumber(intNumber) {
  const tab = [];
  for (let i = 0; i <= intNumber; i++) {
    tab.push(i);
  } return tab;
}

console.log(generateIntFrom0ToIntNumber(25));

// exercice 2 : créer une fonction qui prend en paramètre 2 nombres entiers et qui
// affiche tous les entiers entre le premier et le second

console.log('[BOUCLE FOR]: Exo2--------------');

function generateIntFromMinToMax(min, max) {
  const tab = [];
  for (let i = min; i <= max; i++) {
    tab.push(i);
  } return tab;
}

console.log(generateIntFromMinToMax(10, 45));

// exercice 3 : créer une fonction qui génére un tirage de loto avec
// 7 nombres compris entre 1 et 45

console.log('[BOUCLE FOR]: Exo3--------------');

function createRandom(min, max) {
  return Math.floor(Math.random() * (max - min) + min + 1);
}

function generateLoto(min, max, repeat) {
  const tab = [];
  for (let i = min; i <= repeat; i++) {
    tab.push(createRandom(min, max));
  } return tab;
}

console.log(generateLoto(1, 45, 7));

// exercice 4 : générer un tableau contenant des nombres pairs consécutifs,
// le premier nombre du tableau doit être 4,
// on doit arreter de remplir le tableau quand il y a 20 nombres pairs dans le tableau

console.log('[BOUCLE FOR]: Exo4--------------');

function generateArrayOfPairsNumbers() {
  const tab = [];
  for (let i = 4; tab.length < 20; i += 2) {
    tab.push(i);
  } return tab;
}

console.log(generateArrayOfPairsNumbers());

// exercice 5 : générer un array avec 100 objets avec la forme ci dessous, dont les données sont toutes aléatoires

// const person = {firstName: '', lastName: '', age: 0};
// console.log(person);

// bonus : calculer la moyenne des âges de personnes en utilisant un reduce

console.log('[BOUCLE FOR]: Exo5--------------');

const firstName = [
  'Abdou',
  'Anis',
  'Carmen',
  'Djibril',
  'Edwige',
  'Farida',
  'Frédéric',
  'Grégory',
  'Olivier',
  'Said',
  'William',
];

const lastName = [
  'Abdou',
  'Anis',
  'Carmen',
  'Djibril',
  'Edwige',
  'Farida',
  'Frédéric',
  'Grégory',
  'Olivier',
  'Said',
  'William',
];

function getValue(value) {
  return value[createRandom(0, value.length - 1)];
}

function createArrayOf100Persons() {
  const tab = [];
  for (let i = 0; i < 100; i++) {
    tab.push({
      firstName: getValue(firstName),
      lastName: getValue(lastName),
      age: createRandom(18, 65),
    });
  } return tab;
}

function averageAge() {
  const totalAge = createArrayOf100Persons()
      .reduce((acc, userAge) => acc + userAge.age, 0);
  return totalAge / 100;
}

console.log(createArrayOf100Persons(), averageAge());

// exercice 6 : Écrire un programme qui affiche les nombres de 1 à 199 avec
// un console log.
// Mais pour les multiples de 3, afficher “Fizz” au lieu du nombre et pour les multiples de 5 afficher “Buzz”.
// Pour les nombres multiples de 3 et 5, afficher “FizzBuzz”.
// INFO Importante : cet exercice est le plus utilisé au monde dans les tests

console.log('[BOUCLE FOR]: Exo6--------------');

function fizzBuzz() {
  const tab = [];
  let result = '';
  for (let i = 1; i <= 199; i++, result = '') {
    if (i % 3 === 0) {
      result += 'Fizz';
    }
    if (i % 5 === 0) {
      result += 'Buzz';
    }
    tab.push(result || i);
  }
  return tab;
}

console.log(fizzBuzz());

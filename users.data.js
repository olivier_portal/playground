export const me = {
  firstName: 'Olivier',
  lastName: 'Portal',
  age: 39,
};

const user = {
  firstName: 'Bernard',
  lastName: 'Minet',
  age: 24,
};

const user2 = {
  firstName: 'Joe',
  lastName: 'Lachance',
  age: 49,
};

const user3 = {
  firstName: 'Antoine',
  lastName: 'Durieux',
  age: 56,
};

const user4 = {
  firstName: 'Bernadette',
  lastName: 'Charlac',
  age: 23,
};

const user5 = {
  firstName: 'Bobbye',
  lastName: 'la teigne',
  age: 23,
};

export const tab = [me, user, user2, user3, user4, user5];



// on fait une fonction qui copie la liste des hotels pour pouvoir la réutiliser
// autant qu'on le veut sans la modifier
import {getHostel} from './hotels.data';

// exercice 0 : mettre une majuscule à toutes les RoomName

console.log('[ARRAY]Exo0 : ---------------------------');

function capi(name) {
  return name.charAt(0).toUpperCase() + name.slice(1);
}

function capiAll(hostels) {
  return hostels.map((hostel) => {
    hostel.rooms.map((room) => {
      room.roomName = capi(room.roomName);
      return room;
    });
    return hostel;
  });
}

console.log(capiAll(getHostel()));

// exercice 1 : trier les hotels par nombre
// de chambres (plus grand en 1er) et créer un tableau
// contenant seulement
// le nom des hotels dans leur ordre de tri

console.log('[ARRAY]Exo1 : ', '');

function sortHostelsByRoomsNumber(hostels) {
  return hostels
      .sort((a, b) => b.roomNumbers - a.roomNumbers)
      .map((hostel) => hostel.name);
}

console.log(sortHostelsByRoomsNumber(getHostel()));

// exercice 2 : faire un tableau avec toutes les chambres de tous les hotels,
// et ne garder que les chambres qui
// ont plus que 3 places ou exactement 3 places et
// les classer par ordre alphabétique selon le non de la chambre

console.log('[ARRAY]Exo2 : ', '');

function getAllRoomsInArray(hostels) {
  return hostels
      .reduce((acc, hostel) => acc.concat(hostel.rooms), [])
      .filter((room) => room.size >= 3)
      .sort((a, b) => b.roomName < a.roomName ? 1 : -1);
}

console.log(getAllRoomsInArray(getHostel()));

// exercice 3 : faire un tableau avec toutes les chambres des hotels
// et ne garder que les chambres qui ont plus de 3 places et dont le
// nom de chambre a une taille supérieure à 15 caractères

console.log('[ARRAY]Exo3 : ', '');

function getAllRoomsInArray2(hostels) {
  return hostels
      .reduce((acc, hostel) => acc.concat(hostel.rooms), [])
      .filter((room) => room.size > 3 && room.roomName.length > 15);
}

console.log(getAllRoomsInArray2(getHostel()));

// exercice 4 : enlever de la liste des hotels toutes les chambres qui ont plus
// de 3 places et changer la valeur de roomNumbers pour qu'elle reflete
// le nouveau nombre de chambres

console.log('[ARRAY]Exo4 : ', '');

function suppressRoomsOfMoreThan3(hostels) {
  return hostels
      .map((hostel) => {
        hostel.rooms = hostel.rooms.filter((room) => room.size < 4);
        hostel.roomNumbers = hostel.rooms.length;
        return hostel;
      });
}

console.log(suppressRoomsOfMoreThan3(getHostel()));

// exercice 5  : extraire du tableau hostels l'hotel qui a le nom 'hotel ocean' en le supprimant du tableau,
// et le mettre dans une nouvelle variable
// puis effacer toutes ses chambres et mettre à jour sa valeur room number, puis pusher l'hotel modifié dans hostels
// puis faire un sort par nom d'hotel
// puis donner le nouvel index de l'hotel océan (faire 2 méthodes : avec indexOf et avec un foreach)

console.log('[ARRAY]Exo5 : ', '');

const hostels = getHostel();

const hostelToFind = hostels.findIndex(
    (hostel) => hostel.name === 'hotel ocean');

const [extractHotelOcean] = hostels
    .splice(hostelToFind, 1)
    .map((hostel) => {
      hostel.rooms = [];
      hostel.roomNumbers = hostel.rooms.length;
      return hostel;
    });

hostels.push(extractHotelOcean);

hostels.sort((a, b) => b.name < a.name ? 1 : -1);

const newIndex = hostels.findIndex((hostel) => hostel.name === 'hotel ocean');

console.log(extractHotelOcean, hostels, newIndex);

// exercice 6 : créer un objet dont les clés sont le nom des hotels et dont la valeur est un booléen qui indique
// si l'hotel a une chambre qui s'appelle 'suite marseillaise'

console.log('[ARRAY]Exo6 : ', '');

const keys = {};

const hostels2 = getHostel()
    .map((hostel) => {
      keys[hostel.name] = hostel.rooms
          .some((room) => room.roomName === 'suite marseillaise');
      return hostel;
    });

console.log(keys, hostels2);

// exercice 7 : faire une fonction qui prend en paramètre un id d'hotel et qui retourne son nom

console.log('[ARRAY]Exo7 : ', '');

function getHostelById(hostels, hostelId) {
  const hostelToFind = hostels.find((hostel) => hostel.id === hostelId);
  if (!hostelToFind) {
    return `Cet hôtel n'existe pas`;
  }
  return hostelToFind.name;
}

console.log(getHostelById(getHostel(), 1));
console.log(getHostelById(getHostel(), 4));

// exercice 8 : faire une fonction qui prend en paramètre un id d'hotel et un id de chambre et qui retourne son nom

console.log('[ARRAY]Exo8 : ', '');

function getRoomlById(hostels, hostelId, roomId) {
  const hostelToFind = hostels.find((hostel) => hostel.id === hostelId);
  if (!hostelToFind) {
    return `Cet hôtel n'existe pas`;
  }
  const roomToFind = hostelToFind.rooms.find((room) => room.id === roomId);
  if (!roomToFind) {
    return `Cette chambre n'existe pas`;
  }
  return roomToFind.roomName;
}

console.log(getRoomlById(getHostel(), 1, 6));
console.log(getRoomlById(getHostel(), 2, 26));
console.log(getRoomlById(getHostel(), 4, 6));

// exercice 9: faire une fonction qui prend en paramètre la liste des hotels
// et qui vérifie que toutes les chambres des hotels ont bien
// une majuscule a leur nom et qui renvoie un boolean donnant le résultat.
// Puis faire en sorte que la liste des hotels valide bien cette fonction.

console.log('[ARRAY]Exo9 : ', '');

function verifyAllRoomsAreCapi(hostels) {
  return hostels
      .every((hostel) => hostel.rooms
          .every((room) => room.roomName === capi(room.roomName)));
}

console.log(verifyAllRoomsAreCapi(
    capiAll(getHostel())),
capiAll(getHostel()),
verifyAllRoomsAreCapi(getHostel()),
getHostel(),
);

// exercice 10 : faire une fonction qui prend en paramètre la liste des hotels
// et qui renvoie un tableau contenant les id des hotels
// qui ont au moins une chambre avec plus ou exactement 5 places

console.log('[ARRAY]Exo10 : ', '');

function have5RoomsOrMore(hostel) {
  return hostel.rooms.some((room) => room.size >= 5);
}

function getHostelByIdWith5OrMore(hostels) {
  return hostels.reduce((acc, hostel) => {
    if (have5RoomsOrMore(hostel)) {
      return acc.concat(hostel.id);
    } return acc;
  }, []);
}

console.log(getHostelByIdWith5OrMore(getHostel()), getHostel());

// exercice 11 : faire une fonction qui prend en paramètre la liste des hotels
// et qui renvoie dans un tableau toutes les chambres qui ont plus de 3 places
// et qui sont dans un hotel avec piscine
// puis qui ajoute à chaque chambre une propriété "hotelName"
// qui contient le nom de l'hotel à laquelle elle appartient

console.log('[ARRAY]Exo11 : ', '');

function getAllRoomsWithMoreThan3(hostels) {
  return hostels
      .filter((hostel) => hostel.pool === true)
      .reduce((acc, hostel) => {
        hostel.rooms = hostel.rooms
            .filter((room) => room.size > 3)
            .map((room) => {
              room.hostelName = hostel.name;
              return room;
            });
        return acc.concat(hostel.rooms);
      }, []);
}

console.log(getAllRoomsWithMoreThan3(getHostel()));

// exercice 12 : faire une fonction qui prend en paramètre la liste des hotel,
// un id d'hotel et une taille et qui renvoie le nom et l'id de toutes
// les chambres de cet hotel qui font exactement la taille indiquée

console.log('[ARRAY]Exo12 : ', '');

function getHostelByNameAndId(hostels, hostelId, roomSize) {
  const hostelToFind = hostels.find((hostel) => hostel.id === hostelId);
  if (!hostelToFind) {
    return `Cet hôtel n'existe pas`;
  }
  const sizeToFind = hostelToFind.rooms.find((room) => room.size === roomSize);
  if (!sizeToFind) {
    return `Cet hôtel n'a pas de chambre de cette taille`;
  }
  return hostelToFind.rooms
      .filter((room) => room.size === roomSize)
      .map((room) => ({name: room.roomName, id: room.id}));
}

console.log(getHostelByNameAndId(getHostel(), 1, 4));
console.log(getHostelByNameAndId(getHostel(), 1, 2));
console.log(getHostelByNameAndId(getHostel(), 1, 6));
console.log(getHostelByNameAndId(getHostel(), 4, 3));

// exercice 13 : faire une fonction qui prend en paramètre la liste des hotel et
// un id d'hotel et qui supprime cet hotel de la liste des hotels

console.log('[ARRAY]Exo13 : ', '');

function suppressHostelById(hostels, hostelId) {
  const hostelToFind = hostels.find((hostel) => hostel.id === hostelId);
  if (!hostelToFind) {
    return `Cet hôtel n'existe pas`;
  }
  return hostels.filter((hostel) => hostel.id !== hostelId);
}

console.log((suppressHostelById(getHostel(), 1)));

// exercice 14 : faire une fonction qui prend en paramètre la liste des hotel et
// un id d'hotel et un id de chambre et qui supprime cet chambre de l'hotel concerné

console.log('[ARRAY]Exo14 : ', '');

function suppressRoomByHostelAndRoomId(hostels, hostelId, roomId) {
  const hostelToFind = hostels.find((hostel) => hostel.id === hostelId);
  if (!hostelToFind) {
    return `Cet hôtel n'existe pas`;
  }
  const roomToFind = hostelToFind.rooms.find((room) => room.id === roomId);
  if (!roomToFind) {
    return `Cette chambre n'existe pas`;
  }
  return hostelToFind.rooms.filter((room) => room.id !== roomId);
}

console.log((suppressRoomByHostelAndRoomId(getHostel(), 1, 6)));
console.log((suppressRoomByHostelAndRoomId(getHostel(), 2, 44)));
console.log((suppressRoomByHostelAndRoomId(getHostel(), 5, 4)));

// exercice 15 : faire une fonction qui prend en paramètre la liste des hotels,
// une id d'hotel et un boolean et qui va changer la valeur de l'attribut "pool"
// dans l'hotel concerné avec le boolean donné

console.log('[ARRAY]Exo15 : ', '');

function changePoolValue(hostels, hostelId, poolValue) {
  const hostelToFind = hostels.find((hostel) => hostel.id === hostelId);
  if (!hostelToFind) {
    return `Cet hôtel n'existe pas`;
  }
  hostelToFind.pool = poolValue;
  return hostelToFind;
}

console.log(changePoolValue(getHostel(), 3, false), getHostel());

// exercice 16 : faire une fonction qui prend en paramètre la liste des hotels,
// une id d'hotel et une nouvelle chambre (avec son nom et sa taille) qui
// ajoute la nouvelle chambre dans l'hotel concerné et lui donne un id qui suit le
// dernier id de l'hotel

console.log('[ARRAY]Exo16 : ', '');

function addAnewRoomInHostelById(hostels, hostelId, newRoom) {
  const hostelToFind = hostels.find((hostel) => hostel.id === hostelId);
  if (!hostelToFind) {
    return `Cet hôtel n'existe pas`;
  }
  hostelToFind.rooms.push({...newRoom, id: hostelToFind.rooms.length + 1});
  hostelToFind.roomNumbers = hostelToFind.rooms.length;
  return hostelToFind;
}

const newRoom = {
  roomName: 'Ma nouvelle chambre',
  size: 4,
};

console.log(addAnewRoomInHostelById(getHostel(), 3, newRoom));

// exercice 17 : faire une fonction qui prend en paramètre la liste des hotels,
// une id d'hotel, un id de chambre et un nouveau nom de chambre qui va changer le nom
// de la chambre indiquée par le nouveau nom

console.log('[ARRAY]Exo17 : ', '');

function changeRoomNameById(hostels, hostelId, roomId, newRoomName) {
  const hostelToFind = hostels.find((hostel) => hostel.id === hostelId);
  if (!hostelToFind) {
    return `Cet hôtel n'existe pas`;
  }
  const roomToFind = hostelToFind.rooms.find((room) => room.id === roomId);
  if (!roomToFind) {
    return `Cette chambre n'existe pas`;
  }
  roomToFind.roomName = newRoomName;
  return hostels;
}

console.log(changeRoomNameById(getHostel(), 3, 5, 'toto'));

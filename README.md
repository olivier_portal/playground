## Install
`npm i`

## Run livereload

`npm run serve`

## Index

* [Exercices array](https://gitlab.com/olivier_portal/playingground/-/blob/master/array.js)
* [Exercices object](https://gitlab.com/olivier_portal/playingground/-/blob/master/object.js)
* [Exercices boucles for](https://gitlab.com/olivier_portal/playingground/-/blob/master/for.js)
